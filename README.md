
Project explation:
question no 1 and 2 source codes files is present in side the floder. please open those source code in jupyter, so  that you will be able to run it. in case the file does not run, you can find the answer in this Readme.md file. Also i have attached screen shot in the floder, you can  also refer to those screen shot .

1.Write a regex to extract all the numbers with orange color background from the below text in italics.


{"orders":[{"id":1},{"id":2},{"id":3},{"id":4},{"id":5},{"id":6},{"id":7},{"id":8},{"id":9},{"id":10},{"id":11},{"id":648},{"id":649},{"id":650},{"id":651},{"id":652},{"id":653}],"errors":[{"code":3,"message":"[PHP Warning #2] count(): Parameter must be an array or an object that implements Countable (153)"}]}


answer:{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "a='{\"orders\":[{\"id\":1},{\"id\":2},{\"id\":3},{\"id\":4},{\"id\":5},{\"id\":6},{\"id\":7},{\"id\":8},{\"id\":9},{\"id\":10},{\"id\":11},{\"id\":648},{\"id\":649},{\"id\":650},{\"id\":651},{\"id\":652},{\"id\":653}],\"errors\":[{\"code\":3,\"message\":\"[PHP Warning #2] count(): Parameter must be an array or an object that implements Countable (153)\"}]}'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'{\"orders\":[{\"id\":1},{\"id\":2},{\"id\":3},{\"id\":4},{\"id\":5},{\"id\":6},{\"id\":7},{\"id\":8},{\"id\":9},{\"id\":10},{\"id\":11},{\"id\":648},{\"id\":649},{\"id\":650},{\"id\":651},{\"id\":652},{\"id\":653}],\"errors\":[{\"code\":3,\"message\":\"[PHP Warning #2] count(): Parameter must be an array or an object that implements Countable (153)\"}]}'"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "a"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [],
   "source": [
    "import json\n",
    "b=json.loads(str(a))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "{'orders': [{'id': 1},\n",
       "  {'id': 2},\n",
       "  {'id': 3},\n",
       "  {'id': 4},\n",
       "  {'id': 5},\n",
       "  {'id': 6},\n",
       "  {'id': 7},\n",
       "  {'id': 8},\n",
       "  {'id': 9},\n",
       "  {'id': 10},\n",
       "  {'id': 11},\n",
       "  {'id': 648},\n",
       "  {'id': 649},\n",
       "  {'id': 650},\n",
       "  {'id': 651},\n",
       "  {'id': 652},\n",
       "  {'id': 653}],\n",
       " 'errors': [{'code': 3,\n",
       "   'message': '[PHP Warning #2] count(): Parameter must be an array or an object that implements Countable (153)'}]}"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "b"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 152,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "\"[{'id': 1}, {'id': 2}, {'id': 3}, {'id': 4}, {'id': 5}, {'id': 6}, {'id': 7}, {'id': 8}, {'id': 9}, {'id': 10}, {'id': 11}, {'id': 648}, {'id': 649}, {'id': 650}, {'id': 651}, {'id': 652}, {'id': 653}]\""
      ]
     },
     "execution_count": 152,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "c=str(b['orders'])\n",
    "c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 154,
   "metadata": {},
   "outputs": [],
   "source": [
    "import re"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 166,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "['1',\n",
       " '2',\n",
       " '3',\n",
       " '4',\n",
       " '5',\n",
       " '6',\n",
       " '7',\n",
       " '8',\n",
       " '9',\n",
       " '10',\n",
       " '11',\n",
       " '648',\n",
       " '649',\n",
       " '650',\n",
       " '651',\n",
       " '652',\n",
       " '653']"
      ]
     },
     "execution_count": 166,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "l=re.findall('\\d+',c)\n",
    "l"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 161,
   "metadata": {},
   "outputs": [],
   "source": [
    "d=str(b['errors'])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 165,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'3'"
      ]
     },
     "execution_count": 165,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "e=re.findall('\\d+',d)[0]\n",
    "e"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 167,
   "metadata": {},
   "outputs": [],
   "source": [
    "l.append(e)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 168,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "['1',\n",
       " '2',\n",
       " '3',\n",
       " '4',\n",
       " '5',\n",
       " '6',\n",
       " '7',\n",
       " '8',\n",
       " '9',\n",
       " '10',\n",
       " '11',\n",
       " '648',\n",
       " '649',\n",
       " '650',\n",
       " '651',\n",
       " '652',\n",
       " '653',\n",
       " '3']"
      ]
     },
     "execution_count": 168,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "l"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 169,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 648, 649, 650, 651, 652, 653, 3]"
      ]
     },
     "execution_count": 169,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "final_list=[int(i) for i in l]\n",
    "final_list"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}




2.Here’s the list of reviews of Chrome apps - scraped from Playstore.  DataSet Link

Problem statement - There are times when a user writes Good, Nice App or any other positive text, in the review and gives 1-star rating.
Your goal is to identify the reviews where the semantics of review text does not match rating. 



answer:{
  "nbformat": 4,
  "nbformat_minor": 0,
  "metadata": {
    "colab": {
      "name": "Untitled44.ipynb",
      "provenance": [],
      "collapsed_sections": []
    },
    "kernelspec": {
      "name": "python3",
      "display_name": "Python 3"
    },
    "language_info": {
      "name": "python"
    }
  },
  "cells": [
    {
      "cell_type": "code",
      "metadata": {
        "id": "_nLDL66izaG_",
        "colab": {
          "base_uri": "https://localhost:8080/"
        },
        "outputId": "b4f22f49-bd2d-46e0-e7f3-05bbfc8c6ddb"
      },
      "source": [
        "\n",
        "#Load the libraries\n",
        "import numpy as np\n",
        "import pandas as pd\n",
        "import seaborn as sns\n",
        "import matplotlib.pyplot as plt\n",
        "import nltk\n",
        "nltk.download('punkt')\n",
        "from textblob import TextBlob\n",
        "from textblob import Word\n"
      ],
      "execution_count": 1,
      "outputs": [
        {
          "output_type": "stream",
          "text": [
            "[nltk_data] Downloading package punkt to /root/nltk_data...\n",
            "[nltk_data]   Package punkt is already up-to-date!\n"
          ],
          "name": "stdout"
        }
      ]
    },
    {
      "cell_type": "code",
      "metadata": {
        "colab": {
          "base_uri": "https://localhost:8080/",
          "height": 548
        },
        "id": "a3865d-ez40m",
        "outputId": "ec0675d3-9503-4412-f7a7-418f95f0cfbc"
      },
      "source": [
        "#importing the training data\n",
        "chrome_reviews=pd.read_csv('/content/drive/MyDrive/chrome_reviews.csv')\n",
        "print(chrome_reviews.shape)\n",
        "chrome_reviews.head(10)"
      ],
      "execution_count": 2,
      "outputs": [
        {
          "output_type": "stream",
          "text": [
            "(7204, 10)\n"
          ],
          "name": "stdout"
        },
        {
          "output_type": "execute_result",
          "data": {
            "text/html": [
              "<div>\n",
              "<style scoped>\n",
              "    .dataframe tbody tr th:only-of-type {\n",
              "        vertical-align: middle;\n",
              "    }\n",
              "\n",
              "    .dataframe tbody tr th {\n",
              "        vertical-align: top;\n",
              "    }\n",
              "\n",
              "    .dataframe thead th {\n",
              "        text-align: right;\n",
              "    }\n",
              "</style>\n",
              "<table border=\"1\" class=\"dataframe\">\n",
              "  <thead>\n",
              "    <tr style=\"text-align: right;\">\n",
              "      <th></th>\n",
              "      <th>ID</th>\n",
              "      <th>Review URL</th>\n",
              "      <th>Text</th>\n",
              "      <th>Star</th>\n",
              "      <th>Thumbs Up</th>\n",
              "      <th>User Name</th>\n",
              "      <th>Developer Reply</th>\n",
              "      <th>Version</th>\n",
              "      <th>Review Date</th>\n",
              "      <th>App ID</th>\n",
              "    </tr>\n",
              "  </thead>\n",
              "  <tbody>\n",
              "    <tr>\n",
              "      <th>0</th>\n",
              "      <td>3886</td>\n",
              "      <td>https://play.google.com/store/apps/details?id=...</td>\n",
              "      <td>This is very helpfull aap.</td>\n",
              "      <td>5</td>\n",
              "      <td>0</td>\n",
              "      <td>INDIAN Knowledge</td>\n",
              "      <td>NaN</td>\n",
              "      <td>83.0.4103.106</td>\n",
              "      <td>2020-12-19</td>\n",
              "      <td>com.android.chrome</td>\n",
              "    </tr>\n",
              "    <tr>\n",
              "      <th>1</th>\n",
              "      <td>3887</td>\n",
              "      <td>https://play.google.com/store/apps/details?id=...</td>\n",
              "      <td>Good</td>\n",
              "      <td>3</td>\n",
              "      <td>2</td>\n",
              "      <td>Ijeoma Happiness</td>\n",
              "      <td>NaN</td>\n",
              "      <td>85.0.4183.127</td>\n",
              "      <td>2020-12-19</td>\n",
              "      <td>com.android.chrome</td>\n",
              "    </tr>\n",
              "    <tr>\n",
              "      <th>2</th>\n",
              "      <td>3888</td>\n",
              "      <td>https://play.google.com/store/apps/details?id=...</td>\n",
              "      <td>Not able to update. Neither able to uninstall.</td>\n",
              "      <td>1</td>\n",
              "      <td>0</td>\n",
              "      <td>Priti D BtCFs-29</td>\n",
              "      <td>NaN</td>\n",
              "      <td>85.0.4183.127</td>\n",
              "      <td>2020-12-19</td>\n",
              "      <td>com.android.chrome</td>\n",
              "    </tr>\n",
              "    <tr>\n",
              "      <th>3</th>\n",
              "      <td>3889</td>\n",
              "      <td>https://play.google.com/store/apps/details?id=...</td>\n",
              "      <td>Nice app</td>\n",
              "      <td>4</td>\n",
              "      <td>0</td>\n",
              "      <td>Ajeet Raja</td>\n",
              "      <td>NaN</td>\n",
              "      <td>77.0.3865.116</td>\n",
              "      <td>2020-12-19</td>\n",
              "      <td>com.android.chrome</td>\n",
              "    </tr>\n",
              "    <tr>\n",
              "      <th>4</th>\n",
              "      <td>3890</td>\n",
              "      <td>https://play.google.com/store/apps/details?id=...</td>\n",
              "      <td>Many unwanted ads</td>\n",
              "      <td>1</td>\n",
              "      <td>0</td>\n",
              "      <td>Rams Mp</td>\n",
              "      <td>NaN</td>\n",
              "      <td>87.0.4280.66</td>\n",
              "      <td>2020-12-19</td>\n",
              "      <td>com.android.chrome</td>\n",
              "    </tr>\n",
              "    <tr>\n",
              "      <th>5</th>\n",
              "      <td>3891</td>\n",
              "      <td>https://play.google.com/store/apps/details?id=...</td>\n",
              "      <td>This app good</td>\n",
              "      <td>4</td>\n",
              "      <td>0</td>\n",
              "      <td>Akash More</td>\n",
              "      <td>NaN</td>\n",
              "      <td>83.0.4103.101</td>\n",
              "      <td>2020-12-19</td>\n",
              "      <td>com.android.chrome</td>\n",
              "    </tr>\n",
              "    <tr>\n",
              "      <th>6</th>\n",
              "      <td>3892</td>\n",
              "      <td>https://play.google.com/store/apps/details?id=...</td>\n",
              "      <td>Yes yes</td>\n",
              "      <td>5</td>\n",
              "      <td>0</td>\n",
              "      <td>Leslie Harrison</td>\n",
              "      <td>NaN</td>\n",
              "      <td>87.0.4280.101</td>\n",
              "      <td>2020-12-19</td>\n",
              "      <td>com.android.chrome</td>\n",
              "    </tr>\n",
              "    <tr>\n",
              "      <th>7</th>\n",
              "      <td>3893</td>\n",
              "      <td>https://play.google.com/store/apps/details?id=...</td>\n",
              "      <td>Awesome</td>\n",
              "      <td>5</td>\n",
              "      <td>0</td>\n",
              "      <td>Monish Sangani</td>\n",
              "      <td>NaN</td>\n",
              "      <td>85.0.4183.127</td>\n",
              "      <td>2020-12-19</td>\n",
              "      <td>com.android.chrome</td>\n",
              "    </tr>\n",
              "    <tr>\n",
              "      <th>8</th>\n",
              "      <td>3894</td>\n",
              "      <td>https://play.google.com/store/apps/details?id=...</td>\n",
              "      <td>Very bad app 😞</td>\n",
              "      <td>1</td>\n",
              "      <td>0</td>\n",
              "      <td>Akshat Bhardwaj</td>\n",
              "      <td>NaN</td>\n",
              "      <td>78.0.3904.96</td>\n",
              "      <td>2020-12-19</td>\n",
              "      <td>com.android.chrome</td>\n",
              "    </tr>\n",
              "    <tr>\n",
              "      <th>9</th>\n",
              "      <td>3895</td>\n",
              "      <td>https://play.google.com/store/apps/details?id=...</td>\n",
              "      <td>Many times I tried to update its not updating....</td>\n",
              "      <td>1</td>\n",
              "      <td>0</td>\n",
              "      <td>Aditi Rathor</td>\n",
              "      <td>NaN</td>\n",
              "      <td>86.0.4240.198</td>\n",
              "      <td>2020-12-19</td>\n",
              "      <td>com.android.chrome</td>\n",
              "    </tr>\n",
              "  </tbody>\n",
              "</table>\n",
              "</div>"
            ],
            "text/plain": [
              "     ID  ...              App ID\n",
              "0  3886  ...  com.android.chrome\n",
              "1  3887  ...  com.android.chrome\n",
              "2  3888  ...  com.android.chrome\n",
              "3  3889  ...  com.android.chrome\n",
              "4  3890  ...  com.android.chrome\n",
              "5  3891  ...  com.android.chrome\n",


(TextBlob is a Python (2 and 3) library for processing textual data. It provides a simple API for diving into common natural language processing (NLP)
 tasks such as part-of-speech tagging, noun phrase extraction, sentiment analysis, classification, translation, and more.)



3. Ranking Data - Understanding the co-relation between keyword 
rankings with description or any other attribute. Here’s the dataset. 

i-Is there any co-relation between short description, long description and ranking? Does the placement of keyword 
(for example - using a keyword in the first 10 words - have any co-relation with the ranking)?

answer: No,rather it depends on the reviews, ratings and popularity. the short description and long descripition helps to find relatable 
apps on web store. let's say we search for a specific  keyword, if that keyword matches any data with the chrome app database then result 
will show relatable result based on their ranking. 

ii-Does APP ID (Also known as package name) play any role in ranking? 

ans: No.


